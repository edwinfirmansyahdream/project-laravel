<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
   public function index(){
       return view('halaman.form');
   }
   public function kirim(Request $request){
       $firstname = $request['firstname'];
       $lastname = $request['lastname'];

       return view('halaman.welcome', compact('firstname', 'lastname'));
   }
}
