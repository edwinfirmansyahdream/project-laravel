<!DOCTYPE html>
<html  lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="with=device-width, unitial-scale=1.0">
    <meta http-equal="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
    {{-- <form action="/kirim" method="POST">
        @csrf
        <label for="">Nama Anda</label><br>
        <input type="text" name="yourname" id=""><br>
        <label for="">Alamat</label><br>
        <textarea name="address" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Submit">
    </form> --}}
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" > Male <br>
        <input type="radio" name="gender" > Female <br>
        <input type="radio" name="gender" > Others <br><br>
        <label>Nationally:</label> <br><br>
        <select name="negara"> 
            <option value="indonesia"> Indonesian</option>
            <option value="english"> English</option> 
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" > Bahasa Indonesia <br>
        <input type="checkbox" > English <br>
        <input type="checkbox" > Others<br><br>
        <label>Bio:</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="Submit" value="Sign Up">
    </form>
</body>
</html>